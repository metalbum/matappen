//
//  Downloader.h
//  Matappen
//
//  Created by Anders on 2015-03-23.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Downloader : NSObject

-(void)downloadData:(id)sender withSearchString:(NSString*)string;

@end
