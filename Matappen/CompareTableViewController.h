//
//  CompareTableViewController.h
//  Matappen
//
//  Created by Anders on 2015-03-22.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompareTableViewController : UITableViewController <UISearchControllerDelegate>

@end
