//
//  Food.m
//  Matappen
//
//  Created by Anders on 2015-03-14.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import "Food.h"

@implementation Food

-(Food*)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self != nil) {
        
        self.nutrientNumber = [decoder decodeIntForKey:@"nutrientNumber"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.energykJ = [decoder decodeIntForKey:@"energykJ"];
        self.energykcal = [decoder decodeIntForKey:@"energykcal"];
        self.fat = [decoder decodeIntForKey:@"fat"];
        self.carbs = [decoder decodeIntForKey:@"carbs"];
        self.sugars = [decoder decodeIntForKey:@"sugars"];
        self.protein = [decoder decodeIntForKey:@"protein"];
        self.salt = [decoder decodeIntForKey:@"salt"];
        self.imagePath = [decoder decodeObjectForKey:@"imagePath"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)coder {
    
    [coder encodeInt:self.nutrientNumber forKey:@"nutrientNumber"];
    [coder encodeObject:self.name forKey:@"name"];
    [coder encodeInt:self.energykJ forKey:@"energykJ"];
    [coder encodeInt:self.energykcal forKey:@"energykcal"];
    [coder encodeInt:self.fat forKey:@"fat"];
    [coder encodeInt:self.carbs forKey:@"carbs"];
    [coder encodeInt:self.sugars forKey:@"sugars"];
    [coder encodeInt:self.protein forKey:@"protein"];
    [coder encodeInt:self.salt forKey:@"salt"];
    [coder encodeObject:self.imagePath forKey:@"imagePath"];
}

@end
