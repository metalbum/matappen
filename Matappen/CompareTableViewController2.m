//
//  CompareTableViewController.m
//  Matappen
//
//  Created by Anders on 2015-03-22.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import "CompareTableViewController2.h"
#import "CompareViewController.h"
#import "Food.h"

@interface CompareTableViewController2 ()

@property (nonatomic) NSArray *filteredSearchResults;

@end

@implementation CompareTableViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.tableView) {
        return self.searchResults.count;
    } else {
        return self.filteredSearchResults.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellId = nil;
    NSArray *temp;
    
    if (tableView == self.tableView) {
        temp = self.searchResults;
        cellId = @"CompareTableCell";
    } else {
        temp = self.filteredSearchResults;
        cellId = @"CompareResultCell";
    }
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    
    cell.textLabel.text = temp[indexPath.row][@"name"];
    
    return cell;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if (searchText.length > 0) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.name contains[c] %@", searchText];
        
        self.filteredSearchResults = [self.searchResults filteredArrayUsingPredicate:predicate];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    CompareViewController *detailsVC = [segue destinationViewController];
    
    if ([segue.identifier isEqualToString:@"fromCompare2TableCell"]) {
        
        detailsVC.searchStringOne = self.searchStringOne;
        
        detailsVC.searchStringTwo = [NSString stringWithFormat:@"http://www.matapi.se/foodstuff/%@", self.searchResults[[[self.tableView indexPathForSelectedRow] row]][@"number"]];
        
    } else if ([segue.identifier isEqualToString:@"fromCompare2ResultCell"]) {
        
        detailsVC.searchStringOne = self.searchStringOne;
        
        detailsVC.searchStringTwo = [NSString stringWithFormat:@"http://www.matapi.se/foodstuff/%@", self.filteredSearchResults[self.searchDisplayController.searchResultsTableView.indexPathForSelectedRow.row][@"number"]];
    }
}


@end
