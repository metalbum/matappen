//
//  CompareTableViewController.h
//  Matappen
//
//  Created by Anders on 2015-03-22.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompareTableViewController2 : UITableViewController <UISearchControllerDelegate>

@property (nonatomic) NSString *searchStringOne;

@property (nonatomic) NSArray *searchResults;

@end
