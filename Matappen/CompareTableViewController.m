//
//  CompareTableViewController.m
//  Matappen
//
//  Created by Anders on 2015-03-22.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import "CompareTableViewController.h"
#import "CompareTableViewController2.h"
#import "Food.h"
#import "Downloader.h"

@interface CompareTableViewController ()

@property (nonatomic) NSArray *searchResults;
@property (nonatomic) NSArray *filteredSearchResults;

@end

@implementation CompareTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    Downloader *downloader = [[Downloader alloc] init];
    
    [downloader downloadData:self withSearchString:@"http://www.matapi.se/foodstuff?query="];
}

-(void)onDownloaded:(NSArray*)result {
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    
    self.searchResults = [[result sortedArrayUsingDescriptors:@[sort]] mutableCopy];
    
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.tableView) {
        return self.searchResults.count;
    } else {
        return self.filteredSearchResults.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellId = nil;
    NSArray *temp;
    
    if (tableView == self.tableView) {
        temp = self.searchResults;
        cellId = @"CompareTableCell";
    } else {
        temp = self.filteredSearchResults;
        cellId = @"CompareResultCell";
    }
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    
    cell.textLabel.text = temp[indexPath.row][@"name"];
    
    return cell;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if (searchText.length > 0) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.name contains[c] %@", searchText];
        
        self.filteredSearchResults = [self.searchResults filteredArrayUsingPredicate:predicate];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    CompareTableViewController2 *detailsVC = [segue destinationViewController];
    
    if ([segue.identifier isEqualToString:@"fromCompareTableCell"]) {
        
        detailsVC.searchStringOne = [NSString stringWithFormat:@"http://www.matapi.se/foodstuff/%@", self.searchResults[[[self.tableView indexPathForSelectedRow] row]][@"number"]];
        
    } else if ([segue.identifier isEqualToString:@"fromCompareResultCell"]) {
        
        detailsVC.searchStringOne = [NSString stringWithFormat:@"http://www.matapi.se/foodstuff/%@", self.filteredSearchResults[self.searchDisplayController.searchResultsTableView.indexPathForSelectedRow.row][@"number"]];
    }
    
    detailsVC.searchResults = self.searchResults;

}


@end
