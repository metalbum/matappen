//
//  CustomTableViewCell.h
//  Matappen
//
//  Created by Anders on 2015-03-18.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nutrientName;
@property (weak, nonatomic) IBOutlet UILabel *nutrientCarbs;
@property (weak, nonatomic) IBOutlet UILabel *nutrientProtein;
@property (weak, nonatomic) IBOutlet UILabel *nutrientFat;

@end
