//
//  Food.h
//  Matappen
//
//  Created by Anders on 2015-03-14.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Food : NSObject

@property (nonatomic) int nutrientNumber;
@property (nonatomic) NSString *name;
@property (nonatomic) int energykJ;
@property (nonatomic) int energykcal;
@property (nonatomic) int fat;
@property (nonatomic) int carbs;
@property (nonatomic) int sugars;
@property (nonatomic) int protein;
@property (nonatomic) int salt;
@property (nonatomic) NSString *imagePath;

@end
