//
//  DetailsViewController.h
//  Matappen
//
//  Created by Anders on 2015-03-17.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Food.h"

@interface DetailsViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic) NSString *searchString;

-(void)onDownloaded:(NSDictionary*)result;

@end
