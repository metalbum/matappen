//
//  CompareViewController.h
//  Matappen
//
//  Created by Anders on 2015-03-21.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GKBarGraph.h>

@interface CompareViewController : UIViewController <GKBarGraphDataSource>

@property (nonatomic) NSString *searchStringOne;
@property (nonatomic) NSString *searchStringTwo;

-(void)onDownloaded:(NSDictionary*)result;

@end

