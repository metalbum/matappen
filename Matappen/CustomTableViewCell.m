//
//  CustomTableViewCell.m
//  Matappen
//
//  Created by Anders on 2015-03-18.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
