//
//  Downloader.m
//  Matappen
//
//  Created by Anders on 2015-03-23.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import "Downloader.h"
#import "DetailsViewController.h"
#import "SearchTableViewController.h"

@implementation Downloader

-(void)downloadData:(id)sender withSearchString:(NSString*)string {
    
    NSURL *url = [NSURL URLWithString:string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!error) {
            
            NSError *parsingError;
            
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parsingError];
            
            if (!parsingError) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [sender onDownloaded:result];
                }); 
            }
        } else {
            NSLog(@"Error: %@", error);
        }
    }];
    
    [task resume];
    
}

@end
