//
//  DetailsViewController.m
//  Matappen
//
//  Created by Anders on 2015-03-17.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import "DetailsViewController.h"
#import "Food.h"
#import "Downloader.h"

@interface DetailsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *nutrientNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *energykJLabel;
@property (weak, nonatomic) IBOutlet UILabel *energyKcalLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (weak, nonatomic) IBOutlet UILabel *carbsLabel;
@property (weak, nonatomic) IBOutlet UILabel *sugarsLabel;
@property (weak, nonatomic) IBOutlet UILabel *proteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *saltLabel;
@property (weak, nonatomic) IBOutlet UIButton *addToFavouritesButton;
@property (weak, nonatomic) IBOutlet UIButton *removeFromFavouritesButton;
@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;

@property (weak, nonatomic) IBOutlet UILabel *totalCaloriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *badCaloriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *badCaloriesPercentageLabel;
@property (weak, nonatomic) IBOutlet UILabel *goodOrBadLabel;

@property (weak, nonatomic) IBOutlet UIImageView *nutrientImageView;

@property (weak, nonatomic) IBOutlet UIView *starContainer;
@property (weak, nonatomic) IBOutlet UIImageView *starOne;
@property (weak, nonatomic) IBOutlet UIImageView *starTwo;
@property (weak, nonatomic) IBOutlet UIImageView *starThree;
@property (weak, nonatomic) IBOutlet UIImageView *starFour;

@property (nonatomic) float graphContainerHeight;
@property (nonatomic) float graphContainerWidth;

@property (nonatomic) Food *food;

@property (nonatomic) int goodCalories;
@property (nonatomic) int badCalories;

@property (nonatomic) BOOL addedInFavourites;

@property (nonatomic) NSMutableArray *allFavourites;

@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.allFavourites = [[NSMutableArray alloc] init];
    
    [self setInitialStateOfViews];
    
//    [self setUpAnimator];
    
    [self setUpButtons:NO];
    
    Downloader *downloader = [[Downloader alloc] init];
    
    [downloader downloadData:self withSearchString:self.searchString];
}

-(void)setUpAnimator {
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.starContainer];
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.starOne, self.starTwo, self.starThree, self.starFour]];
    [self.animator addBehavior:self.gravity];
    
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.starOne, self.starTwo, self.starThree, self.starFour]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior:self.collision];
}

-(void)onDownloaded:(NSDictionary*)result {
    
    [self initFood:result];
    
    [self updateLabels];
    
    [self loadFavourites];
    
    [self checkIfExistsInFavourites];
    
    [self setUpButtons:YES];
    
    [self animate];
    
    [self calculateNutritionalValue];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setInitialStateOfViews {
    
    self.nutrientNameLabel.text = @"Laddar...";
    
    self.takePhotoButton.alpha = 0;
    self.addToFavouritesButton.alpha = 0;
    self.removeFromFavouritesButton.alpha = 0;
    
    self.energykJLabel.alpha = 0;
    self.energyKcalLabel.alpha = 0;
    self.fatLabel.alpha = 0;
    self.carbsLabel.alpha = 0;
    self.sugarsLabel.alpha = 0;
    self.proteinLabel.alpha = 0;
    self.saltLabel.alpha = 0;
    
    self.starContainer.alpha = 0;
    
    self.totalCaloriesLabel.alpha = 0;
    self.badCaloriesLabel.alpha = 0;
    self.badCaloriesPercentageLabel.alpha = 0;
    self.goodOrBadLabel.alpha = 0;
    
}


-(void)initFood:(NSDictionary*)result {
    
    self.food = [[Food alloc] init];
    
    self.food.nutrientNumber = (int)[result[@"number"] integerValue];
    self.food.name = result[@"name"];
    self.food.energykJ = (int)[result[@"nutrientValues"][@"energyKj"] integerValue];
    self.food.energykcal = (int)[result[@"nutrientValues"][@"energyKcal"] integerValue];
    self.food.fat = (int)[result[@"nutrientValues"][@"fat"] integerValue];
    self.food.carbs = (int)[result[@"nutrientValues"][@"carbohydrates"] integerValue];
    self.food.sugars = (int)[result[@"nutrientValues"][@"saccharose"] integerValue];
    self.food.protein = (int)[result[@"nutrientValues"][@"protein"] integerValue];
    self.food.salt = (int)[result[@"nutrientValues"][@"salt"] integerValue];
    self.food.imagePath = [self createImagePath];
    
}

-(void)updateLabels {
    
    [self.nutrientNameLabel setText:self.food.name];
    
    self.energykJLabel.text = [NSString stringWithFormat:@"Energi, kJ: %d", (int) self.food.energykJ];
    self.energyKcalLabel.text = [NSString stringWithFormat:@"Energi, kcal: %d", (int) self.food.energykcal];
    self.fatLabel.text = [NSString stringWithFormat:@"Fett: %dg", (int) self.food.fat];
    self.carbsLabel.text = [NSString stringWithFormat:@"Kolhydrater: %dg", (int) self.food.carbs];
    self.sugarsLabel.text = [NSString stringWithFormat:@"Sockerarter: %dg", (int) self.food.sugars];
    self.proteinLabel.text = [NSString stringWithFormat:@"Protein: %dg", (int) self.food.protein];
    self.saltLabel.text = [NSString stringWithFormat:@"Salter: %dg", (int) self.food.salt];
    
    UIImage *image = [UIImage imageWithContentsOfFile:self.food.imagePath];
    
    if (image.imageOrientation != UIImageOrientationRight) {
        
        image = [UIImage imageWithCGImage:image.CGImage scale:1 orientation:UIImageOrientationRight];
        
    }
    
    if (image) {
        self.nutrientImageView.image = image;
    }
    
}

-(void)checkIfExistsInFavourites {
    
    for (Food *f in self.allFavourites) {
        
        if (f.nutrientNumber == self.food.nutrientNumber) {
            self.addedInFavourites = YES;
            break;
        }
    }
}

-(void)setUpButtons:(BOOL)finishedDownloadingData {
    
    if (!finishedDownloadingData) {
        self.takePhotoButton.enabled = NO;
        
        self.addToFavouritesButton.enabled = NO;
        self.addToFavouritesButton.hidden = YES;
        
        self.removeFromFavouritesButton.enabled = NO;
        self.removeFromFavouritesButton.hidden = YES;
        
    } else {
        self.takePhotoButton.enabled = YES;
        
        if (self.addedInFavourites) {
            self.removeFromFavouritesButton.enabled = YES;
            self.removeFromFavouritesButton.hidden = NO;
            
            self.addToFavouritesButton.enabled = NO;
            self.addToFavouritesButton.hidden = YES;
        } else {
            self.removeFromFavouritesButton.enabled = NO;
            self.removeFromFavouritesButton.hidden = YES;
            
            self.addToFavouritesButton.enabled = YES;
            self.addToFavouritesButton.hidden = NO;
        }
    }
}

-(void)animate {
    
    [UIView beginAnimations:nil context:nil];
    self.energykJLabel.alpha = 1;
    self.energyKcalLabel.alpha = 1;
    self.fatLabel.alpha = 1;
    self.carbsLabel.alpha = 1;
    self.sugarsLabel.alpha = 1;
    self.proteinLabel.alpha = 1;
    self.saltLabel.alpha = 1;
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    self.starContainer.alpha = 1;
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    self.totalCaloriesLabel.alpha = 1;
    self.badCaloriesLabel.alpha = 1;
    self.badCaloriesPercentageLabel.alpha = 1;
    self.goodOrBadLabel.alpha = 1;
    [UIView commitAnimations];
    
    
    [self setUpAnimator];
    
    [UIView beginAnimations:nil context:nil];
    
    self.addToFavouritesButton.alpha = 1;
    self.removeFromFavouritesButton.alpha = 1;
    
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    
    self.takePhotoButton.alpha = 1;
    
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    
    self.nutrientImageView.alpha = 1;
    
    [UIView commitAnimations];
    
}

- (IBAction)addRemoveButtonPressed:(id)sender {
    
    BOOL add;
    
    if (self.addedInFavourites) {
        self.addedInFavourites = NO;
        add = NO;
    } else {
        self.addedInFavourites = YES;
        add = YES;
    }
    
    [self updateFavourites:add];
    
    [self setUpButtons:YES];
    
}
- (IBAction)takePhotoButtonPressed:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *i = info[UIImagePickerControllerOriginalImage];
    
    UIImage *image = [UIImage imageWithCGImage:i.CGImage scale:1 orientation:UIImageOrientationRight];
    
    self.nutrientImageView.image = image;
    
    [self saveImage:image];
}

-(void)saveImage:(UIImage*)image {
    
    NSData *data = UIImagePNGRepresentation(self.nutrientImageView.image);
    BOOL success = [data writeToFile:self.food.imagePath atomically:YES];
    
    if (!success) {
        NSLog(@"Failed to save image");
    }
    
}

-(NSString*)createImagePath {
    NSArray *dirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentDirectory = dirs[0];
    
    NSString *imagePath = [documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"nutrient_nr_%d_image.png", self.food.nutrientNumber]];
    
    return imagePath;
    
}

-(void)loadFavourites {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSData *dataToRepresentTask = [defaults objectForKey:@"favourites"];
    NSArray *loadedTask = [NSKeyedUnarchiver unarchiveObjectWithData:dataToRepresentTask];
    
    NSArray *temp = [[NSArray alloc] initWithArray:loadedTask];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    
    self.allFavourites = [[temp sortedArrayUsingDescriptors:@[sort]] mutableCopy];
    
}

-(void)updateFavourites:(BOOL)add {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (add) {
        [self.allFavourites addObject:self.food];
        
        
    } else {
        for (int i = 0; i < self.allFavourites.count; i++) {
            
            if (self.food.nutrientNumber == [self.allFavourites[i] nutrientNumber]) {
                self.addedInFavourites = NO;
                
                [self.allFavourites removeObjectAtIndex:i];
                break;
            }
        }
    }
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.allFavourites];
    
    [defaults setObject:data forKey:@"favourites"];
    [defaults synchronize];
    
}

-(void)calculateNutritionalValue {
    
    self.badCalories = self.food.carbs + self.food.fat;
    
    self.goodCalories = self.food.energykcal - self.badCalories;
    
    int totalCalories = self.goodCalories + self.badCalories;
    
    int temp = self.badCalories * 100;
    
    int temp2 = totalCalories * 100;
    
    float badCaloriesPercentage = (float)temp / (float)temp2 * 100;
    
    self.totalCaloriesLabel.text = [NSString stringWithFormat:@"Totala kalorier: %d", totalCalories];
    self.badCaloriesLabel.text = [NSString stringWithFormat:@"Varav \"Dåliga\": %d", self.badCalories];
    self.badCaloriesPercentageLabel.text = [NSString stringWithFormat:@"%d procent dåliga", (int)badCaloriesPercentage];
    if (badCaloriesPercentage > 20) {
        self.goodOrBadLabel.text = @"Onyttigt!";
    } else {
        self.goodOrBadLabel.text = @"Nyttigt!";
    }
    
    [self animateStars:badCaloriesPercentage];
    
}

-(void)animateStars:(float)badCaloriesPercentage {
    
    NSString *imageName = @"star_gold";
    
    if (badCaloriesPercentage < 5) {
        self.starOne.image = [UIImage imageNamed:imageName];
        self.starTwo.image = [UIImage imageNamed:imageName];
        self.starThree.image = [UIImage imageNamed:imageName];
        self.starFour.image = [UIImage imageNamed:imageName];
    } else if (badCaloriesPercentage < 10) {
        self.starOne.image = [UIImage imageNamed:imageName];
        self.starTwo.image = [UIImage imageNamed:imageName];
        self.starThree.image = [UIImage imageNamed:imageName];
    } else if (badCaloriesPercentage < 15) {
        self.starOne.image = [UIImage imageNamed:imageName];
        self.starTwo.image = [UIImage imageNamed:imageName];
    } else if (badCaloriesPercentage < 20) {
        self.starOne.image = [UIImage imageNamed:imageName];
    } else {
        
    }
}

@end
