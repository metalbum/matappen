//
//  CompareViewController.m
//  Matappen
//
//  Created by Anders on 2015-03-21.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import "CompareViewController.h"
#import "Food.h"
#import "Downloader.h"

@interface CompareViewController ()

@property (weak, nonatomic) IBOutlet UILabel *foodNumberOneLabel;
@property (weak, nonatomic) IBOutlet UILabel *foodNumberTwoLabel;

@property (weak, nonatomic) IBOutlet GKBarGraph *graph;

@property (nonatomic) NSMutableArray *foods;

@end

@implementation CompareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.graph.dataSource = self;
    
    self.foods = [[NSMutableArray alloc] init];
    
    Downloader *downloader = [[Downloader alloc] init];
    
    [downloader downloadData:self withSearchString:self.searchStringOne];
    [downloader downloadData:self withSearchString:self.searchStringTwo];
    
    
}

-(void)onDownloaded:(NSDictionary*)result {
    
    [self initFood:result];
    
    if (self.foods.count == 2) {
        
        [self setUpLabels];
        
        [self.graph draw];
        
    }
    
}

-(void)initFood:(NSDictionary*)result {
    
    Food *food = [[Food alloc] init];
    
    food.nutrientNumber = (int)[result[@"number"] integerValue];
    food.name = result[@"name"];
    food.energykJ = (int)[result[@"nutrientValues"][@"energyKj"] integerValue];
    food.energykcal = (int)[result[@"nutrientValues"][@"energyKcal"] integerValue];
    food.fat = (int)[result[@"nutrientValues"][@"fat"] integerValue];
    food.carbs = (int)[result[@"nutrientValues"][@"carbohydrates"] integerValue];
    food.sugars = (int)[result[@"nutrientValues"][@"saccharose"] integerValue];
    food.protein = (int)[result[@"nutrientValues"][@"protein"] integerValue];
    food.salt = (int)[result[@"nutrientValues"][@"salt"] integerValue];
    
    [self.foods addObject:food];
    
}

-(void)setUpLabels {
    
    self.foodNumberOneLabel.text = [self.foods[0] name];
    self.foodNumberTwoLabel.text = [self.foods[1] name];
    
}

- (NSInteger)numberOfBars {
    return 6;
}

- (NSNumber *)valueForBarAtIndex:(NSInteger)index {
    
    NSNumber *value = nil;
    
    switch (index) {
            
        case 0:
            value = [NSNumber numberWithInt:[self.foods[0] carbs]];
            break;
        
        case 1:
            value = [NSNumber numberWithInt:[self.foods[1] carbs]];
            break;
            
        case 2:
            value = [NSNumber numberWithInt:[self.foods[0] fat]];
            break;
            
        case 3:
            value = [NSNumber numberWithInt:[self.foods[1] fat]];
            break;
            
        case 4:
            value = [NSNumber numberWithInt:[self.foods[0] protein]];
            break;
            
        case 5:
            value = [NSNumber numberWithInt:[self.foods[1] protein]];
            
            break;
    }
    
    return value;
}

- (UIColor *)colorForBarAtIndex:(NSInteger)index {
    
    
    if (index % 2) {
        return [UIColor redColor];
    } else {
        return [UIColor blueColor];
    }
}

- (UIColor *)colorForBarBackgroundAtIndex:(NSInteger)index {
    
    return [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
}

- (CFTimeInterval)animationDurationForBarAtIndex:(NSInteger)index {
    return 0.5f;
}

- (NSString *)titleForBarAtIndex:(NSInteger)index {
    
    int value = 0;
    
    switch (index) {
            
        case 0:
            value = [self.foods[0] carbs];
            break;
            
        case 1:
            value = [self.foods[1] carbs];
            break;
            
        case 2:
            value = [self.foods[0] fat];
            break;
            
        case 3:
            value = [self.foods[1] fat];
            break;
            
        case 4:
            value = [self.foods[0] protein];
            break;
            
        case 5:
            value = [self.foods[1] protein];
            
            break;
    }
    
    return [NSString stringWithFormat:@"%dg", value];
}

@end
