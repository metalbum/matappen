//
//  FavouritesTableViewController.m
//  Matappen
//
//  Created by Anders on 2015-03-19.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import "FavouritesTableViewController.h"
#import "CustomTableViewCell.h"
#import "Food.h"
#import "DetailsViewController.h"

@interface FavouritesTableViewController ()

@property (nonatomic) NSArray *favourites;

@end

@implementation FavouritesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadFavourites];
}

-(void)loadFavourites {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSData *dataToRepresentTask = [defaults objectForKey:@"favourites"];
    
    //Logs -[NSKeyedUnarchiver initForReadingWithData:]: data is NULL if @"favourites" is NULL
    NSArray *loadedTask = [NSKeyedUnarchiver unarchiveObjectWithData:dataToRepresentTask];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    
    self.favourites = [[loadedTask sortedArrayUsingDescriptors:@[sort]] mutableCopy];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.favourites.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"FavouritesTableCell" forIndexPath:indexPath];
    
    cell.nutrientName.text = [self.favourites[indexPath.row] name];
    cell.nutrientCarbs.text = [NSString stringWithFormat:@"Kolhydrater: %dg", [self.favourites[indexPath.row] carbs]];
    cell.nutrientProtein.text = [NSString stringWithFormat:@"Protein: %dg", [self.favourites[indexPath.row] protein]];
    cell.nutrientFat.text = [NSString stringWithFormat:@"Fett: %dg", [self.favourites[indexPath.row] fat]];
    
    return cell;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [self loadFavourites];
    
    [self.tableView reloadData];
}

#pragma mark - Navigation

 
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    DetailsViewController *destinationView = [segue destinationViewController];
    
    destinationView.searchString = [NSString stringWithFormat:@"http://www.matapi.se/foodstuff/%d", [self.favourites[[[self.tableView indexPathForSelectedRow] row]] nutrientNumber]];
    
}


@end
