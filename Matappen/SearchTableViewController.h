//
//  SearchTableViewController.h
//  Matappen
//
//  Created by Anders on 2015-03-12.
//  Copyright (c) 2015 Anders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewController : UITableViewController <UISearchControllerDelegate>

@property (nonatomic) NSArray *searchResults;

-(void)onDownloaded:(NSArray*)result;

@end
